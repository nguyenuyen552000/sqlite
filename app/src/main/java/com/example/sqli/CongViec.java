package com.example.sqli;

public class CongViec {

    private int IdCv;
    private String TenCv;

    public CongViec(int idCv, String tenCv) {
        IdCv = idCv;
        TenCv = tenCv;
    }

    public int getIdCv() {
        return IdCv;
    }

    public void setIdCv(int idCv) {
        IdCv = idCv;
    }

    public String getTenCv() {
        return TenCv;
    }

    public void setTenCv(String tenCv) {
        TenCv = tenCv;
    }
}
