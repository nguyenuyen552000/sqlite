package com.example.sqli;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ListView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    Database database;
    ListView lv_congviec;
    ArrayList<CongViec> arrayJob;
    jobAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv_congviec = findViewById(R.id.listviewJob);
        arrayJob = new ArrayList<>();
        adapter = new jobAdapter(this, R.layout.row_job, arrayJob);
        lv_congviec.setAdapter(adapter);


        //tạo database ghichu
        database = new Database(this, "ghichu.sqlite", null, 1);
        //tạo bảng điểm
        database.QueyData("CREATE TABLE IF NOT EXISTS CongViec(Id INTEGER PRIMARY KEY AUTOINCREMENT, NameCv VARCHAR(200))");
//INSERT DATA
        //  database.QueyData("INSERT INTO CongViec VALUES(null,'Nhớ học Tiếng Anh nha')");

        getDataJob();
    }

public   void DialogDeleteJob(final String nameJob , final int id){
    AlertDialog.Builder dialogDelete=new AlertDialog.Builder(this);
    dialogDelete.setMessage("Do you want to delete "+nameJob+" job?");
    dialogDelete.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
database.QueyData("DELETE FROM CongViec WHERE Id='"+ id +"'");
            Toast.makeText(MainActivity.this, "DELETE SUCCESSFULLY "+nameJob, Toast.LENGTH_SHORT).show();
getDataJob();
        }
    });
dialogDelete.setNegativeButton("NO", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialog, int which) {

    }
});
    dialogDelete.show();

}
public void  DialogUpdateJob(String name, final int id){
        final Dialog dialog =new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update);
        final EditText ediNameJob=(EditText) dialog.findViewById(R.id.editNameJobUpdata);
        Button btnUpdate=(Button) dialog.findViewById(R.id.buttonUpdata);
        Button btnexxit=(Button) dialog.findViewById(R.id.buttonExit) ;

        ediNameJob.setText(name);
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameUPdate=ediNameJob.getText().toString().trim();
                database.QueyData("UPDATE CongViec SET NameCv = '"+nameUPdate+"' WHERE Id= '"+id+"' ");
                Toast.makeText(MainActivity.this, " successfully Updata", Toast.LENGTH_SHORT).show();
dialog.dismiss();
getDataJob();
            }
        });

        btnexxit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
}


    private void getDataJob(){
        //SELECT DATA
        Cursor datacongviev =database.GetData("SELECT * FROM CongViec");
        arrayJob.clear();
        while(datacongviev.moveToNext()){
            String name=datacongviev.getString(1);
            int id =datacongviev.getInt(0);
            arrayJob.add(new CongViec(id,name));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.add_job,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item) {

if(item.getItemId()==R.id.add_menu){
    DialogAdd();
}


        return super.onOptionsItemSelected(item);
    }
    private void DialogAdd(){
        final Dialog dialog =new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
     dialog.setContentView(R.layout.dialog_add_job);

        final EditText editName=(EditText) dialog.findViewById(R.id.editNameJob);
        Button buttonAdd=(Button) dialog.findViewById(R.id.buttonAdd);
        Button buttonExit=(Button) dialog.findViewById(R.id.buttonExit);


        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String nameJob= editName.getText().toString();
if(nameJob.equals("")){
    Toast.makeText(MainActivity.this, "", Toast.LENGTH_SHORT).show();
}else{
    database.QueyData("INSERT INTO CongViec VALUES(null,'"+nameJob+"')");
    Toast.makeText(MainActivity.this, "Successfully added", Toast.LENGTH_SHORT).show();
dialog.dismiss();
getDataJob();
}
            }
        });













        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
     dialog.show();

   //     dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }
}