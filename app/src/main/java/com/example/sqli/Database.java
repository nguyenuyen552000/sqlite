package com.example.sqli;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.widget.CursorAdapter;

import androidx.annotation.Nullable;

public class Database extends SQLiteOpenHelper {
    public Database(Context context,  String name,  SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    //truy van khong trả kết quả :create,insert ,update,delete...
    public void QueyData(String sql){
        SQLiteDatabase database =getWritableDatabase();
        database.execSQL(sql);
    }
//truy vấn có kết quả SELECT
    public Cursor GetData(String sql){
SQLiteDatabase database =getReadableDatabase();
return database.rawQuery(sql,null);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
