package com.example.sqli;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class jobAdapter  extends BaseAdapter {
    private MainActivity context1;
    private int layout;
    private List<CongViec> jobList;

    public jobAdapter(MainActivity context, int layout, List<CongViec> jobList) {
        this.context1 = context;
        this.layout = layout;
        this.jobList = jobList;
    }

    @Override
    public int getCount() {
        return jobList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    private class ViewHolder{
        TextView txtnamejob;
        ImageView imgDelete,imgEdit;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      ViewHolder holder;
       if(convertView==null)
       {
           holder =new ViewHolder();
           LayoutInflater inflater= (LayoutInflater) context1.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
           convertView=inflater.inflate(layout,null);
holder.txtnamejob=convertView.findViewById(R.id.tv_namejob);
holder.imgDelete=convertView.findViewById(R.id.img_delete);
holder.imgEdit=convertView.findViewById(R.id.img_edit);

convertView.setTag(holder);
       }else {
           holder=(ViewHolder) convertView.getTag();
       }
final CongViec congViec=jobList.get(position);
       holder.txtnamejob.setText(congViec.getTenCv());
//bat su kien xoa va sua
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(context, "sủa ", Toast.LENGTH_SHORT).show();
            context1.DialogUpdateJob(congViec.getTenCv(),congViec.getIdCv());

            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Toast.makeText(context1, "bạn muốn xoá nó à đợi chút nheé", Toast.LENGTH_SHORT).show();
            context1.DialogDeleteJob(congViec.getTenCv(),congViec.getIdCv());


            }
        });
        return convertView;
    }
}
